"use strict";

let showingNotification = false; // We don't showing notification if another notification already exists
let notificationQueue = []; // Notification queue

// Our custom alert()
function customAlert(text) {
    if (!showingNotification) {
        showingNotification = true;
        const notification = document.createElement("div");
        notification.classList.add("notification");
        notification.textContent = text;
        document.body.appendChild(notification);

        setTimeout(() => notification.classList.add("notification-appear"), 100);

        setTimeout(() => {
            notification.classList.add("notification-dissappear");
            showingNotification = false;

            if (notificationQueue.length > 0) {
                setTimeout(() => {
                    customAlert(notificationQueue.shift());
                }, 250);
            } // Showing first notification from queue (FIFO)
            setTimeout(() => { document.body.removeChild(notification); }, 1000);
        }, 3000);
    } else {
        notificationQueue.push(text);
    }
}


// Function for submitting words
function submit(event) {
    event.preventDefault();
    const text = textInput.value.trim();

    const data = new FormData();
    data.append("word", text);
    requestInit.body = data;

    if (text != "") {
        event.target.disabled = true;

        fetch(form.action, requestInit).then(() => {
            textInput.value = "";
            textInput.focus();
            console.info(`Send ${text}`);
        }).catch((e) => {
            console.error(e);
            customAlert(`Failed to submit: ${e}`);
        }).finally(() => { event.target.disabled = false; });


    } else {
        customAlert("You can't submit empty text!");
    }
}

// HTML elements
const form = document.getElementById("form");
const textInput = document.getElementById("input");
const submitButton = document.getElementById("send");

submitButton.addEventListener("click", submit);

// Other variables
let requestInit = {
    "method": "POST"
};
