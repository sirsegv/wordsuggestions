import vweb
import os
import rand
import time

struct Server {
	vweb.Context
mut:
	users shared map[string]string
}

fn main() {
	if os.args[1] or {''} == 'list' {
		// When run with 'wordsuggestions list', print all logged word lists
		print_word_lists()
	} else {
		// Run server
		mut s := Server{}
		s.mount_static_folder_at('css', '/words/css')
		s.mount_static_folder_at('js', '/words/js')
		vweb.run(&s, 8080)
	}
}

@['/words/']
fn (mut s Server) page() vweb.Result {
	if !s.check_user() { return s.redirect('/words/new') }

	return s.file('html/page.html')
}

@['/words/new']
fn (mut s Server) newvisitor() vweb.Result {
	return s.file('html/new.html')
}

@['/words/adduser'; post]
fn (mut s Server) adduser(name string) vweb.Result {
	id := rand.string(5)

	lock s.users {
		s.users[id] = name
		s.set_cookie_with_expire_date('id', id, time.now().add(30 * time.minute))
	}
	
	return s.redirect('/words/')
}

@['/words/addword'; post]
fn (mut s Server) addword(word string) vweb.Result {
	if !s.check_user() { return s.redirect('/words/new') }
	
	id := s.get_cookie('id') or {'none'}
	file_path := 'words/${id}.txt'
	
	// Add new file for new user
	if !os.exists(file_path) {
		rlock s.users {
			os.write_file(file_path, s.users[id] + '\n') or { eprintln(err) }
		}
	}

	// Add word to users file
	mut file := os.open_append(file_path) or { return s.redirect('/words/') }
	file.writeln(word) or { eprintln('err') }
	file.close()

	// Reload
	return s.redirect('/words/')
}

// Ensure user is registered
fn (s &Server) check_user() bool {
	id := s.get_cookie('id') or {''}

	rlock s.users {
		return s.users[id] != ''
	}

	return false
}

// Print all logged word lists with the name of the user who submitted them
fn print_word_lists() {
	for i in os.ls('words')or{return} {
		println(os.read_lines('words/${i}')or{continue}[0] + ' ' + i)
	}
}